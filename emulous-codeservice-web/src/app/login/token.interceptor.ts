import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';
import { LoginService } from './login.service';
@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(public login: LoginService) {}
  intercept(request: HttpRequest<any>, next: HttpHandler) {
    const tokenData = this.login.getToken();
    request = request.clone({
      setHeaders: {
        Authorization: `Bearer ${
          !!tokenData ? this.login.getToken().token : ''
        }`,
      },
    });
    return next.handle(request);
  }
}
