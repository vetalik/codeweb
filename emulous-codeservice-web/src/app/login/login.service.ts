import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs/internal/observable/throwError';
import { Router } from '@angular/router';
import { IToken } from './login.interface';
import { Routing } from '../constants/routing.constats';
import { environment } from '../../environments/environment';

@Injectable({ providedIn: 'root' })
export class LoginService {
  private token: IToken;

  constructor(private httpClient: HttpClient, private router: Router) {}

  /**
   * logout
   */
  public logout() {
    this.token = null;
    localStorage.removeItem(Routing.TOKEN);
  }

  public isLoggedIn() {
    return !!this.getToken();
  }

  public getToken() {
    return JSON.parse(localStorage.getItem(Routing.TOKEN));
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    this.token = {};
    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  }

  public login({ username, password }) {
    this.httpClient
      .post(`${environment.api}/api/${Routing.TOKEN}`, {
        Username: username,
        Password: password,
      })
      .pipe(
        map((token: IToken) => {
          localStorage.setItem(Routing.TOKEN, JSON.stringify(token));
          this.router.navigate([Routing.CODES]);
        }),
        catchError(this.handleError)
      )
      .subscribe();
  }
}
