import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { LoginComponent } from './login.component';
import { LoginService } from './login.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [FormsModule, HttpClientModule],
  exports: [LoginComponent],
  declarations: [LoginComponent],
  providers: [LoginService]
})
export class LoginModule {}
