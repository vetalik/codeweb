export interface IToken {
  expirationDate?: string;
  token?: string;
}
