import { Component, OnInit } from '@angular/core';
import { LoginService } from './login.service';

@Component({
  selector: 'login',
  templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {
  public LoginModel = {
    username: '',
    password: ''
  };

  constructor(private loginService: LoginService) {}

  ngOnInit() {}

  public submit(event) {
    this.loginService.login(this.LoginModel);
  }
}
