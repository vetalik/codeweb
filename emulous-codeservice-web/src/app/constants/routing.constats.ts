export enum Routing {
  LOGIN = 'login',
  CODES = 'codes',
  CODE = 'code',
  TOKEN = 'token',
}
