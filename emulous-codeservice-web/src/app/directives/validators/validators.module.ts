import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OneOfDirective } from './one-of.directive';

@NgModule({
  declarations: [OneOfDirective],
  exports: [OneOfDirective],
  imports: [CommonModule],
})
export class ValidatorsModule {}
