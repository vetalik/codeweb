import { Directive, Input } from '@angular/core';
import {
  NG_VALIDATORS,
  Validator,
  FormGroup,
  ValidationErrors,
} from '@angular/forms';
import { OnOfInput } from './one-of.validator';

@Directive({
  selector: '[appOneOf]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: OneOfDirective, multi: true },
  ],
})
export class OneOfDirective implements Validator {
  @Input('appOneOf') oneOf: string[] = [];

  validate(formGroup: FormGroup): ValidationErrors {
    return OnOfInput(this.oneOf[0], this.oneOf[1])(formGroup);
  }
}
