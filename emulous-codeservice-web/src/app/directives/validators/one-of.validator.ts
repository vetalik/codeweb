import { FormGroup } from '@angular/forms';

// custom validator to check that either one or other value present, not both, and one none
export function OnOfInput(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
    const leftControl = formGroup.controls[controlName];
    const rightControl = formGroup.controls[matchingControlName];

    if (!leftControl || !rightControl) {
      return null;
    }
    if (!!leftControl.value && !!rightControl.value) {
      leftControl.setErrors({ oneOf: true });
      rightControl.setErrors({ oneOf: true });
    } else if (!leftControl.value && !rightControl.value) {
      leftControl.setErrors({ required: true });
      rightControl.setErrors({ required: true });
    } else {
      leftControl.setErrors(null);
      rightControl.setErrors(null);
      return null;
    }
  };
}
