import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { TestComponent } from './test/test.component';
import { CodesComponent } from './codes/codes.component';
import { JwtGuard } from './guards/jwt.guard';

import { Routing } from './constants/routing.constats';

const routes: Routes = [
  {
    component: CodesComponent,
    path: '',
    canActivate: [JwtGuard]
  },
  {
    component: LoginComponent,
    path: Routing.LOGIN
  },
  {
    component: CodesComponent,
    path: Routing.CODES,
    canActivate: [JwtGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
