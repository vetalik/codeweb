export interface ICodeAdd {
  slug?: string;
  validFrom?: string;
  validTo?: String;
  activatesFor?: number;
  activatesUntil?: String;
  ActivationsLimit?: number;
  description?: string;
  appId: number;
}
