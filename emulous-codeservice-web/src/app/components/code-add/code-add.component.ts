import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ICodeAdd } from './code-add.interfaces';
import { CodeAddService } from 'src/app/rest-services/code-add.service';
import * as moment from 'moment';
import { NgForm } from '@angular/forms';

const initModel: ICodeAdd = {
  appId: 1, // ToDo: make editable later
};
@Component({
  selector: 'app-code-add',
  templateUrl: './code-add.component.html',
  styleUrls: ['./code-add.component.less'],
})
export class CodeAddComponent implements OnInit {
  @Output() public onsubmit: EventEmitter<boolean> = new EventEmitter();
  public model: ICodeAdd = {
    ...initModel,
    validFrom: this.formatDate(),
    validTo: this.formatTomorrow(),
  };
  public config = {
    format: 'YYYY-DD-MM',
  };
  constructor(private codeAdd: CodeAddService) {}

  ngOnInit() {}

  public onSubmit(frm: NgForm) {
    this.codeAdd.addCode(this.model);
    this.model = {
      ...initModel,
      validFrom: this.formatDate(),
      validTo: this.formatTomorrow(),
    };
    frm.resetForm(this.model);

    this.onsubmit.emit();
  }

  public newDate() {
    return new Date();
  }

  public formatDate() {
    const now = moment().format('YYYY-MM-DD');
    return now;
  }
  public formatTomorrow() {
    const now = moment()
      .add(1, 'day')
      .format('YYYY-MM-DD');
    return now;
  }
}
