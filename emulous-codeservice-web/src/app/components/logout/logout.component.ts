import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/login/login.service';
import { Router } from '@angular/router';
import { Routing } from 'src/app/constants/routing.constats';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.less'],
})
export class LogoutComponent implements OnInit {
  constructor(public loginService: LoginService, private router: Router) {}

  ngOnInit() {}

  public logout() {
    this.loginService.logout();
    this.router.navigate([Routing.LOGIN]);
  }
}
