import {
  Component,
  OnChanges,
  Input,
  SimpleChanges,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { CodeListService } from 'src/app/rest-services';
import * as changeCase from 'change-case';
import { formatDate } from '@angular/common';
import locale2 from 'locale2';
import * as moment from 'moment';
import { ICode } from './interfaces';

@Component({
  selector: 'code-list',
  templateUrl: './code-list.component.html',
  styleUrls: ['./code-list.component.less'],
  encapsulation: ViewEncapsulation.None,
})
export class CodeListComponent implements OnChanges {
  @Input() public codes;
  @Input() public columns;

  @ViewChild('myTable') table: any;

  public columnDefs;
  public locale = locale2;
  public visibleData = [];

  public model = {
    activeOnly: false,
  };
  private gridApi;
  private gridColumnApi;

  rowData = [];
  constructor(public codeListService: CodeListService) {
    this.formatter = this.formatter.bind(this);
  }

  public isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }

  public formatter({ value }: { value: any }) {
    let formatted = value;
    if (!!!value) {
      return value;
    }
    if (this.isNumeric(value)) {
      return value;
    }
    try {
      const dtFormat = new Intl.DateTimeFormat(locale2);
      // formatted = formatDate(value, dtFormat.format(), locale2);
      const m = moment;
      formatted = moment
        .parseZone(value)
        .local()
        .format();
    } catch (error) {
      console.log('Exception????', error);
      // do nothing as we just need to try to get date
    }
    return formatted;
  }

  public isDate(value) {
    if (!value) {
      return false;
    }
    if (this.isNumeric(value)) {
      return false;
    }
    const a = moment
      .parseZone(value)
      .local()
      .format();
    if (a !== 'Invalid date') {
      return true;
    } else {
      return false;
    }
  }

  private isActive(code: ICode) {
    const res =
      Date.parse(code.validFrom) < Date.now() &&
      Date.parse(code.validTo) > Date.now() &&
      code.activatesFor > code.activationsNumber;
    return res;
  }

  public onChange() {
    if (this.model.activeOnly) {
      this.visibleData = this.codes.filter(code => this.isActive(code));
    } else {
      this.visibleData = this.codes;
    }
  }

  public ngOnChanges(changes: SimpleChanges) {
    if (changes.codes) {
      this.codes = changes.codes.currentValue;
      this.visibleData = this.codes;
    }
    if (changes.columns && changes.columns.currentValue) {
      this.columns = changes.columns.currentValue.map(col => {
        return {
          ...col,
          name: col.name,
          prop: col.displayName,
        };
      });
    }
  }

  getColumnHeader(column) {
    return this.columns.filter(col => col.name === column.name)[0].displayName;
  }
  getValueForColumn(column, raw, value) {
    return value;
  }

  onDetailToggle(event) {
    // // ToDo: check if needed
  }

  toggleExpandRow($event, row) {
    $event.preventDefault();
    this.table.rowDetail.toggleExpandRow(row);
  }
}
