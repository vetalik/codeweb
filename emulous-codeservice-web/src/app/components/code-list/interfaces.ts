export interface ICode {
  activatesFor: number;
  activatesUntil: string;
  activationsNumber: number;
  appId: number;
  codeId: number;
  slug: string;
  userId: string;
  validFrom: string;
  validTo: string;
}
