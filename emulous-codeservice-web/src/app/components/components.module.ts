import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CodeAddComponent } from './code-add/code-add.component';
import { CodeListComponent } from './code-list/code-list.component';
import { ValidatorsModule } from '../directives/validators/validators.module';
import { DpDatePickerModule } from 'ng2-date-picker';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { AgGridModule } from 'ag-grid-angular';
import { LogoutComponent } from './logout/logout.component';

@NgModule({
  declarations: [CodeAddComponent, CodeListComponent, LogoutComponent],
  exports: [CodeAddComponent, CodeListComponent, LogoutComponent],
  imports: [
    CommonModule,
    FormsModule,
    ValidatorsModule,
    AgGridModule.withComponents([]),
    DpDatePickerModule,
    NgxDatatableModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class ComponentsModule {}
