import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { TokenInterceptor } from './login/token.interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { TestModule } from './test/test.module';
import { LoginModule } from './login/login.module';
import { CodesModule } from './codes/codes.module';
import { ComponentsModule } from './components/components.module';

import { PROVIDERS } from './rest-services';
// import { OneOfDirective } from './directives/validators/one-of.directive';
import { ValidatorsModule } from './directives/validators/validators.module';

@NgModule({
  declarations: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    BrowserModule,
    AppRoutingModule,
    TestModule,
    LoginModule,
    CodesModule,
    ComponentsModule,
    ValidatorsModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
    }),
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true,
      ...PROVIDERS,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
