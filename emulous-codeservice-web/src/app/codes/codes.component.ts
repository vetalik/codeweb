import { Component, OnInit } from '@angular/core';
import { CodeListService } from '../rest-services';

@Component({
  selector: 'app-codes',
  templateUrl: './codes.component.html',
  styleUrls: ['./codes.component.less'],
})
export class CodesComponent implements OnInit {
  constructor(public codeListService: CodeListService) {}
  public codes;
  public columns;

  ngOnInit() {
    this.codeListService.fetchCodes().subscribe(_ => {
      this.codes = this.codeListService.codes;
      this.columns = this.codeListService.columns;
    });
  }

  public onSubmit() {
    this.codeListService.fetchCodes().subscribe(_ => {
      this.codes = this.codeListService.codes;
      this.columns = this.codeListService.columns;
    });
  }
}
