import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CodesComponent } from './codes.component';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  declarations: [CodesComponent],
  imports: [CommonModule, ComponentsModule],
  exports: [CodesComponent]
})
export class CodesModule {}
