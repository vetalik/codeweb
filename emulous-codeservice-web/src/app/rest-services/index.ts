import { CodeAddService } from './code-add.service';
import { CodeListService } from './code-list.service';
export * from './code-add.service';
export * from './code-list.service';

export const PROVIDERS = [CodeAddService, CodeListService];
