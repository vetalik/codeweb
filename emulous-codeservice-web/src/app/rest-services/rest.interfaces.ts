export interface IColumn {
  name: string;
  displayName: string;
}

export interface Codes {
  codes: Array<object>;
  columns: Array<IColumn>;
}
