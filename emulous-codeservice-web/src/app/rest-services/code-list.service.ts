import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { map, catchError, share } from 'rxjs/operators';
import { throwError } from 'rxjs/internal/observable/throwError';
import { Router } from '@angular/router';
import { Routing } from '../constants/routing.constats';
import { Codes, IColumn } from './rest.interfaces';
import { environment } from '../../environments/environment';

@Injectable({ providedIn: 'root' })
export class CodeListService {
  public codes = [];
  public columns: Array<IColumn> = [];

  constructor(private httpClient: HttpClient, private router: Router) {}

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  }

  public fetchCodes() {
    const client = this.httpClient
      .get<Codes>(`${environment.api}/api/${Routing.CODE}`)
      .pipe(
        map(data => {
          return data;
        }),
        share(),
        catchError(this.handleError)
      );
    client.subscribe(({ codes, columns }) => {
      this.codes = codes;
      this.columns = columns;
    });
    return client;
  }
}
