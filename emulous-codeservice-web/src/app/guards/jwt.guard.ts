import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { LoginService } from '../login/login.service';

@Injectable({
  providedIn: 'root'
})
export class JwtGuard implements CanActivate {
  constructor(private login: LoginService, private router: Router) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    const token = JSON.parse(localStorage.getItem('token'));
    let canGo = true;
    if (token) {
      const exp = moment.utc(token.expirationDate);
      const now = moment.utc();
      canGo = now < exp;
    } else {
      canGo = false;
    }

    if (!canGo) {
      this.router.navigate(['/login'], {
        queryParams: {
          return: state.url
        }
      });
    }
    return canGo;
  }
}
